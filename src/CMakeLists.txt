#Minimum version of CMAKE used
cmake_minimum_required(VERSION 2.8)

list(APPEND CMAKE_CXX_FLAGS "-std=c++0x -g -ftest-coverage -fprofile-arcs -msse -msse2 -msse3 -march=native")
	

#Déclaration du projet
project(UrbanTracker)

find_package(OpenCV REQUIRED)
if(${OpenCV_VERSION} VERSION_LESS 2.3.1)
  message (FATAL_ERROR "OpenCV version is not compatible: ${OpenCV_VERSION}")
endif()

#BGS Lib
set(EXECUTABLE_OUTPUT_PATH bin/${CMAKE_BUILD_TYPE})
add_subdirectory(BGSLib)
include_directories(BGSLib/include)
#External dependancy
add_subdirectory(external)
include_directories(external/cvBlob/include)
include_directories(external/sqlite/include)
include_directories(external/brisk/include)


#BOOST
find_package(Boost REQUIRED program_options filesystem system)
if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
endif()

add_subdirectory(TrackingLib)
include_directories(TrackingLib/include)
add_subdirectory(TrackingApp)


add_subdirectory(MetricsEvaluationLib)
include_directories(MetricsEvaluationLib/include)
add_subdirectory(MetricsEvaluationApp)
add_subdirectory(QtGroundTruthBuilderApp)




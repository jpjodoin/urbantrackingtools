#ifndef APP_CONTEXT_H
#define APP_CONTEXT_H

#include "ObjectTypeManager.h"
#include <vector>
#include <map>

class SceneObject;
class DrawableSceneView;
class InputFrameProviderIface;
class QtGroundTruthBuilderApp;


class AppContext
{
public:
	AppContext(QtGroundTruthBuilderApp* gtBuilderApp);
	virtual ~AppContext();
	void clear();
	InputFrameProviderIface* getVFM() { return mVFM;}
	void setInputFileModule(InputFrameProviderIface* vfm) { mVFM = vfm;}
	SceneObject* getNewObject();

	std::map<unsigned int, DrawableSceneView*>& getTimestampToScene() { return mTimestampToScene;}
	ObjectTypeManager& getObjectTypeManager() { return mObjetManager; }
	std::vector<SceneObject*>& getSceneObjectList() { return mSceneObject;}
	int getObjectIdx(SceneObject* so) const;
	
	void setLastIdOfSet(unsigned int id) {mId = id;}
	int getNewId() {return mId++;}
	
	QtGroundTruthBuilderApp* getGui() { return mGTBuilderApp;}

private:
	std::map<unsigned int, DrawableSceneView*> mTimestampToScene;
	ObjectTypeManager mObjetManager;
	std::vector<SceneObject*> mSceneObject;
	InputFrameProviderIface* mVFM;
	int mId;
	QtGroundTruthBuilderApp* mGTBuilderApp;

};



#endif

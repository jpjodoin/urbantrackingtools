#ifndef GT_IMPORTER_H
#define GT_IMPORTER_H

#include <QString>
#include "BaseTask.h"
#include <opencv2/opencv.hpp>

class AppContext;

class GTImporter : public BaseTask
{
public:
	enum InputType
	{
		DIRECTORY,
		FILE
	};


	GTImporter(StatusProgressBar* progressBarControl, AppContext* appcontext, const QString& name, const QString& extension, const QString& shortName, InputType type, int w, int h);
	virtual ~GTImporter(){}
	bool isOwnedByThread() {return false;}
	const QString& getName() {return mName; }
	const QString& getShortName() {return mShortName;}
	const QString& getExtension() { return mExtension; }
	InputType getType() { return mType; }
	void run();
	virtual void LoadDatabase() = 0;
	virtual bool askHomography() = 0;

	void setLoadPath(const QString& path) { mPath = path;}
	void setHomography(const cv::Mat& m) {mHomography=m.clone();}


protected:
	
	int getVideoHeight() { return mHeight;}
	int getVideoWidth() { return mWidth;}
	AppContext* getAppContext() { return mAppContext;}

	const QString& getLoadPath() const { return mPath;}
	const cv::Mat& getHomography() const { return mHomography;}
private:
	QString mShortName;
	AppContext* mAppContext;
	QString mName;
	QString mExtension;
	InputType mType;
	int mWidth;
	int mHeight;
	QString mPath;
	cv::Mat mHomography;
};

#endif
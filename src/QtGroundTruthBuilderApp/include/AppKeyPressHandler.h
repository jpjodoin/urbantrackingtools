#ifndef APP_KEY_PRESS_HANDLER_H
#define APP_KEY_PRESS_HANDLER_H

#include <set>
#include <QEvent>
#include <QObject>

class QtGroundTruthBuilderApp;

class AppKeyPressHandler : public QObject
 {
     Q_OBJECT
 public:
     AppKeyPressHandler(std::set<Qt::Key> keysToCatch, QtGroundTruthBuilderApp* eventReceiver);
	 virtual ~AppKeyPressHandler() {}
 protected:
     bool eventFilter(QObject *obj, QEvent *event);
	 std::set<Qt::Key> mKeysToCatch;
	 QtGroundTruthBuilderApp* mEventReceiver;
 };



#endif
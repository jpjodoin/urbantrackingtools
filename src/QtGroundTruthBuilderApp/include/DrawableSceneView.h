#ifndef DRAWABLE_SCENE_VIEW_H
#define DRAWABLE_SCENE_VIEW_H

#include <QGraphicsScene>
#include <opencv2/opencv.hpp>
class ObjectObservation;

class AppContext;

class DrawableSceneView : public QGraphicsScene
{
public:
	DrawableSceneView(unsigned int timestamp, QObject* parent, AppContext* context);
	virtual ~DrawableSceneView(){};



	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	virtual void keyPressEvent ( QKeyEvent * event );
	virtual void keyReleaseEvent ( QKeyEvent * event );
	QRectF getRectangle(const QPointF& p1, const QPointF& p2) const;


private:
	bool creatingShape;
	QPointF mStart;
	QPointF mLast;
	ObjectObservation* mCurrentObjectObservation;
	unsigned int mTimestamp;
	AppContext* mContext;

};


#endif
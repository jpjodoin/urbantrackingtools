#/*include "CreateBoundingBoxMouseHandler.h"
#include "BoundingBox.h"
#include <iostream>
#include "Canvas.h"
#include <cstdlib>

CreateBoundingBoxMouseHandler::CreateBoundingBoxMouseHandler(Canvas* c)
: MouseHandler()
, mCanvas(c)
, mColor(rand()%255,rand()%255, rand()%255)
, mStartX(0)
, mStartY(0)
{

}

CreateBoundingBoxMouseHandler::~CreateBoundingBoxMouseHandler()
{

}

void CreateBoundingBoxMouseHandler::onMouseDown(int x, int y)
{
	mStartX = x;
	mStartY = y;
	mLastX = x;
	mLastY = y;
}

void CreateBoundingBoxMouseHandler::onMouseMove(int x, int y)
{
	mLastX = x;
	mLastY = y;
}

void CreateBoundingBoxMouseHandler::onMouseUp(int x, int y)
{
	if(abs(mStartX-x) > 3 && abs(mStartY-y))
	{
		unsigned int X0 = mStartX < mLastX ? mStartX : mLastX;
		unsigned int X1 = X0 == mStartX ? mLastX : mStartX;
		unsigned int Y0 = mStartY < mLastY ? mStartY : mLastY;
		unsigned int Y1 = Y0 == mStartY ? mLastY : mStartY;
		mCanvas->addBoundingBox(BoundingBox(X0, X1, Y0, Y1, mColor));
	}
}

void CreateBoundingBoxMouseHandler::draw(cv::Mat& img)
{
	unsigned int X0 = mStartX < mLastX ? mStartX : mLastX;
	unsigned int X1 = X0 == mStartX ? mLastX : mStartX;
	unsigned int Y0 = mStartY < mLastY ? mStartY : mLastY;
	unsigned int Y1 = Y0 == mStartY ? mLastY : mStartY;
	cv::rectangle(img, cv::Rect(X0, Y0, X1-X0, Y1-Y0), mColor, 3);
}*/
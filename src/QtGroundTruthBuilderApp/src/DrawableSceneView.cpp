#include "DrawableSceneView.h"
#include <QGraphicsSceneMouseEvent>
#include "SceneObject.h"
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include "ObjectObservation.h"
#include <QKeyEvent>
#include "ObjectObservation.h"
#include "AppContext.h"
#include "QtGroundTruthBuilderApp.h"

DrawableSceneView::DrawableSceneView(unsigned int timestamp,QObject* parent, AppContext* context)
: QGraphicsScene(parent)
, mCurrentObjectObservation(nullptr)
, creatingShape(false)
, mTimestamp(timestamp)
, mContext(context)
{

}


void DrawableSceneView::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
	QGraphicsScene::mousePressEvent(event);
	if(selectedItems().empty())
	{
		//Create object Handler
		mStart = event->scenePos();
		mLast = mStart;
		QColor color(0,0,0);//rand()%255,rand()%255, rand()%255);
		mCurrentObjectObservation = new ObjectObservation(color, false);
		mCurrentObjectObservation->setRect(getRectangle(mStart, mLast)); //Todo: Move this in constructor

		/*SceneObject* so = mContext->getGui()->getFrameListCurrentObject();
		if(!so)
			so = mContext->getNewObject(); 
		mCurrentObjectObservation->associateToSceneObject(so);*/
		addItem(mCurrentObjectObservation);
		creatingShape = true;
	}
	else
	{
		creatingShape = false;
	}

	QPointF scenePos = event->scenePos();
	

}

void DrawableSceneView::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	if(creatingShape)
	{
		mLast = event->scenePos();
		mCurrentObjectObservation->setRect(getRectangle(mStart, mLast));
	}	
	else
		QGraphicsScene::mouseMoveEvent(event);
	
}
void DrawableSceneView::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	if(creatingShape)
	{
		mLast = event->scenePos();		
		if(abs(mStart.x()-mLast.x()) > 3 && abs(mStart.y()-mLast.y()) > 3)
		{			
			SceneObject* so = mContext->getGui()->getFrameListCurrentObject();
			if(!so)
				so = mContext->getNewObject(); 
			mCurrentObjectObservation->associateToSceneObject(so);
			mCurrentObjectObservation->setRect(getRectangle(mStart, mLast));
			mCurrentObjectObservation->setSelected(true);
		}
		else
			this->removeItem(mCurrentObjectObservation);
		creatingShape = false;
	}	
	else
		QGraphicsScene::mouseReleaseEvent(event);
	
}

QRectF DrawableSceneView::getRectangle(const QPointF& p1, const QPointF& p2) const
{
	
	float xMin = p1.x();
	float xMax = p2.x();
	float yMin = p1.y();
	float yMax = p2.y();

	if(xMin > xMax)
	{
		xMin = p2.x();
		xMax = p1.x();
	}
	
	if(yMin > yMax)
	{
		yMin = p2.y();
		yMax = p1.y();
	}


	return QRectF(QPointF(xMin, yMin), QPointF(xMax, yMax));
}

void DrawableSceneView::keyPressEvent ( QKeyEvent * event )
{
	QGraphicsScene::keyPressEvent(event);
}
void DrawableSceneView::keyReleaseEvent ( QKeyEvent * event )
{
	bool treated = false;
	if(event->key() == Qt::Key_Delete)
	{
		QList<QGraphicsItem*> itemList = this->selectedItems();
		if(!itemList.empty())
		{
			for (auto it = itemList.begin(); it != itemList.end(); ++it)
			{
				this->removeItem(*it);
				//delete
			}
			treated = true;
		}
	}
	update();
	if(!treated)
		QGraphicsScene::keyReleaseEvent(event);
	
}

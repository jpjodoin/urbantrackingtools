#include "BoundingBoxHandler.h"
#include "BoundingBox.h"

BoundingBoxHandler::BoundingBoxHandler(BoundingBox* boundingBox)
: mBoundingBox(boundingBox)
{

}

BoundingBoxHandler::~BoundingBoxHandler()
{

}

void BoundingBoxHandler::draw(cv::Mat& img)
{
	draw(img, false);
}

HandlerControl BoundingBoxHandler::getSelectedHandler(cv::Mat& background, unsigned int x, unsigned int y)
{
	cv::Mat blankImage(background.size(), CV_MAKETYPE(background.depth(), 1), cv::Scalar(0));
	draw(blankImage, true);
	HandlerControl hc = (HandlerControl) blankImage.at<unsigned char>(y,x);
	if(hc != NONE)
	{
		std::cout << hc << std::endl;
	}
	return hc;
}

void BoundingBoxHandler::draw(cv::Mat& background, bool picking)
{
	unsigned int X0 = mBoundingBox->getStartX();
	unsigned int X1 = mBoundingBox->getEndX();
	unsigned int Y0 = mBoundingBox->getStartY();
	unsigned int Y1 = mBoundingBox->getEndY();
	unsigned int xSz = X1-X0;
	unsigned int ySz = Y1-Y0;

	cv::Scalar manipulatorColor = cv::Scalar(255,0,0,127);
	//Coin sup�rieur gauche
	if(picking)
		manipulatorColor = TOPLEFTCORNER;	
	cv::rectangle(background, cv::Rect(X0-2,Y0-2, 5, 5), manipulatorColor, 5);
	//Coin sup�rieur droit
	if(picking)
		manipulatorColor = TOPRIGHTCORNER;	
	cv::rectangle(background, cv::Rect(X0+xSz-2,Y0-2, 5, 5), manipulatorColor, 5);
	//Coin inf�rieur gauche
	if(picking)
		manipulatorColor = BOTTOMLEFTCORNER;	
	cv::rectangle(background, cv::Rect(X0-2,Y0+ySz-2, 5, 5), manipulatorColor, 5);
	//Coin inf�rieur droit
	if(picking)
		manipulatorColor = BOTTOMRIGHTCORNER;	
	cv::rectangle(background, cv::Rect(X0+xSz-2,Y0+ySz-2, 5, 5), manipulatorColor, 5);

	double scaleHandlerSize = 0.4;
	double scaleHandlerPos = (1-scaleHandlerSize)/2.0;
	//Scale Gauche
	if(picking)
		manipulatorColor = LEFTSCALE;	
	cv::rectangle(background, cv::Rect(X0 -2,Y0+scaleHandlerPos*ySz, 5, scaleHandlerSize*ySz), manipulatorColor, 5);
	//Scale Droit
	if(picking)
		manipulatorColor = RIGHTSCALE;
	cv::rectangle(background, cv::Rect(X0+xSz -2,Y0+scaleHandlerPos*ySz, 5,scaleHandlerSize*ySz), manipulatorColor, 5);
	//Scale Haut
	if(picking)
		manipulatorColor = TOPSCALE;
	cv::rectangle(background, cv::Rect(X0 + scaleHandlerPos*xSz ,Y0-2, scaleHandlerSize*xSz, 5), manipulatorColor, 5);
	//Scale Bas
	if(picking)
		manipulatorColor = BOTTOMSCALE;
	cv::rectangle(background, cv::Rect(X0+scaleHandlerPos*xSz ,Y0+ySz-2, scaleHandlerSize*xSz, 5), manipulatorColor, 5);

	unsigned int radius = (std::min((float)xSz, (float)ySz)/2.0)*0.5;
	cv::Point center = cv::Point(X0+xSz/2, Y0+ySz/2);
	

	if(!picking)
	{
		cv::circle(background, center,radius, manipulatorColor, 3);
		//LeftArrow
		cv::Point endPt = center;
		endPt.x -=radius;
		drawArrow(background, center, endPt, manipulatorColor, ArrowType::LEFTARROW);
		//RightArrow
		endPt = center;
		endPt.x +=radius;
		drawArrow(background, center, endPt, manipulatorColor, ArrowType::RIGHTARROW);
		//UpArrow
		endPt = center;
		endPt.y+=radius;
		drawArrow(background, center, endPt, manipulatorColor, ArrowType::UPARROW);
		//DownArrow
		endPt = center;
		endPt.y-=radius;
		drawArrow(background, center, endPt, manipulatorColor, ArrowType::DOWNARROW);
	}
	else
	{
		if(picking)
			manipulatorColor = TRANSLATION;
		cv::circle(background, center,radius, manipulatorColor, -1); //Cercle plein

	}
	
}


void BoundingBoxHandler::drawArrow(cv::Mat& background, cv::Point startPt, cv::Point endPt, cv::Scalar color, BoundingBoxHandler::ArrowType type)
{
	float thickness = 3;
	cv::line(background, startPt, endPt, color, thickness);
	cv::Point delta = endPt-startPt;

	float arrowHeadLength = 10;
	float arrowHeadSide = 5;

	switch(type)
	{
	case LEFTARROW:
		{
			cv::line(background, cv::Point(endPt.x+arrowHeadLength, endPt.y-arrowHeadSide), endPt, color, thickness);
			cv::line(background, cv::Point(endPt.x+arrowHeadLength, endPt.y+arrowHeadSide), endPt, color, thickness);
		}

		break;

	case RIGHTARROW:
		{		
			cv::line(background, cv::Point(endPt.x-arrowHeadLength, endPt.y-arrowHeadSide), endPt, color, thickness);
			cv::line(background, cv::Point(endPt.x-arrowHeadLength, endPt.y+arrowHeadSide), endPt, color, thickness);
		}
		break;

	case DOWNARROW:
		{
			cv::line(background, cv::Point(endPt.x - arrowHeadSide, endPt.y + arrowHeadLength), endPt, color, thickness);
			cv::line(background, cv::Point(endPt.x + arrowHeadSide, endPt.y + arrowHeadLength), endPt, color, thickness);
		}

		break;

	case UPARROW:
		{
			cv::line(background, cv::Point(endPt.x-arrowHeadSide, endPt.y - arrowHeadLength), endPt, color, thickness);
			cv::line(background, cv::Point(endPt.x+arrowHeadSide, endPt.y - arrowHeadLength), endPt, color, thickness);		
		}

		break;
	}	
}
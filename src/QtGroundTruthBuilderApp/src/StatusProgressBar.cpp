#include "StatusProgressBar.h"
#include <qlabel.h>
#include <qprogressbar.h>
#include <qstatusbar.h>


StatusProgressBar::StatusProgressBar(QStatusBar* statusBar)
: mProgressBar(new QProgressBar())
, mProgressText(new QLabel(""))
{
	mProgressBar->setRange(0, 100);
	statusBar->addWidget(mProgressBar, 1);
	statusBar->addWidget(mProgressText);
	displayProgressControls(false);
}


void StatusProgressBar::displayProgressControls(bool enable)
{
	if(enable)
	{
		mProgressBar->show();
		mProgressText->show();
	}
	else
	{		
		mProgressBar->hide();
		mProgressText->hide();
	}
}

void StatusProgressBar::setProgressPercent(int progress)
{
	mProgressBar->setValue(progress);
}

void StatusProgressBar::setProgressText(QString text)
{
	mProgressText->setText(text);
}
#include "BaseTask.h"
#include "StatusProgressBar.h"

#include <iostream>

BaseTask::BaseTask(StatusProgressBar* progressBarControl, QString taskDescription)
: mProgressBarController(progressBarControl)
, mTaskDescription(taskDescription)
{
	if(mProgressBarController)
	{
		connect(this, SIGNAL(updatePercent(int)), mProgressBarController, SLOT(setProgressPercent(int)));
		connect(this, SIGNAL(updateText(QString)), mProgressBarController, SLOT(setProgressText(QString)));
		connect(this, SIGNAL(displayUi(bool)), mProgressBarController, SLOT(displayProgressControls(bool)));
	}

}
BaseTask::~BaseTask()
{
}

void BaseTask::updateProgressBarPercent(int percent)
{
	emit updatePercent(percent);
}

void BaseTask::enableProgressBar(bool enable)
{
	emit displayUi(enable);
}

void BaseTask::setProgressBarText(QString text)
{
	emit updateText(text);
}
	
void BaseTask::process()
{
	enableProgressBar(true);
	setProgressBarText(mTaskDescription + " started");
	run();
	setProgressBarText(mTaskDescription + " ended");
	enableProgressBar(false);	
	emit finished();
}



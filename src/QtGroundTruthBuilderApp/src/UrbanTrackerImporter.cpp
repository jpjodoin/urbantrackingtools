#include "UrbanTrackerImporter.h"
#include "SQLiteManager.h"
#include "AppContext.h"
#include "SceneObject.h"
#include <map>
#include <iostream>
#include "ObjectObservation.h"
#include "DrawableSceneView.h"
#include "InputVideoFileModule.h"
UrbanTrackerImporter::UrbanTrackerImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight)
: GTImporter(progressBarControl, appContext, "UrbanTrackSQL", "*.sqlite", "UrbanTrackSQL" , GTImporter::FILE, videoWidth, videoHeight)
{

}

UrbanTrackerImporter::~UrbanTrackerImporter()
{

}

void UrbanTrackerImporter::LoadDatabase()
{
	unsigned int maxFrameNumber = 0;
	bool success = false;
	SQLiteManager sql(getLoadPath().toStdString());
	if(sql.isConnected())
	{
		updatePercent(10);
		success = true;
		//1) We create objects_type
		ObjectTypeManager& otm = getAppContext()->getObjectTypeManager();
		//auto sqlite = sql.getDatabase();
		std::vector<std::string> typeList;
		success &= sql.executeStatementGetSingleColumn("SELECT name FROM object_type", typeList);
		if(success)
		{
			for(auto it = typeList.begin(); it!= typeList.end();++it)
				otm.addType(*it);
		}
		updatePercent(15);

		//2) We create objects		
		std::vector<SceneObject*>& sceneObjectList = getAppContext()->getSceneObjectList();
		std::map<std::string, SceneObject*> idToObject;
		std::vector<std::vector<std::string>> objectInfos;
		success &= sql.executeStatementGetArray("SELECT object_id, description, type FROM sceneobject", objectInfos);
		if(success && !objectInfos.empty() && objectInfos[0].size() == 3)
		{

			for (unsigned int row = 0; row <objectInfos.size(); ++row)
			{
				SceneObject* so = new SceneObject(objectInfos[row][0], objectInfos[row][1], objectInfos[row][2]);
				sceneObjectList.push_back(so);
				idToObject.insert(std::pair<std::string, SceneObject*>(objectInfos[row][0], so));
				updatePercent(15 + 5*row/objectInfos.size() );
			}
		}	
		
		//3) We create objects observation
		//std::map<unsigned int, DrawableSceneView*>& timestampToScene = getAppContext()->getTimestampToScene();
		std::vector<std::vector<std::string>> objectObs;
		success &= sql.executeStatementGetArray("SELECT object_id, frame_number, top_left_corner_x, top_left_corner_y, bottom_right_corner_x, bottom_right_corner_y FROM position", objectObs);
		if(success && !objectObs.empty() && objectObs[0].size() == 6)
		{
			for (unsigned int row = 0; row <objectObs.size(); ++row)
			{
				
				const std::string& object_id = objectObs[row][0];
				unsigned int frame_number;
				double top_left_corner_x, top_left_corner_y, bottom_right_corner_x, bottom_right_corner_y;
				success &= Utils::String::StringToType(objectObs[row][1], frame_number);
				if(frame_number > maxFrameNumber)
					maxFrameNumber = frame_number;
				success &= Utils::String::StringToType(objectObs[row][2], top_left_corner_x);
				success &= Utils::String::StringToType(objectObs[row][3], top_left_corner_y);
				success &= Utils::String::StringToType(objectObs[row][4], bottom_right_corner_x);
				success &= Utils::String::StringToType(objectObs[row][5], bottom_right_corner_y);
				auto it = getAppContext()->getTimestampToScene().find(frame_number);
				if(it == getAppContext()->getTimestampToScene().end())
				{
					auto itPair = getAppContext()->getTimestampToScene().insert(std::pair<unsigned int, DrawableSceneView*>(frame_number, new DrawableSceneView(frame_number, nullptr, getAppContext())));
					it = itPair.first;
				}

				ObjectObservation* objObs = new ObjectObservation(QColor(rand()%255,rand()%255, rand()%255), false);
				objObs->setRect(QRectF(top_left_corner_x, top_left_corner_y, bottom_right_corner_x-top_left_corner_x, bottom_right_corner_y-top_left_corner_y));
				
				//This allows us to repair the database
				SceneObject* so = nullptr;
				auto objectIt = idToObject.find(object_id);
				if(objectIt == idToObject.end())
				{
					so = new SceneObject(object_id, "", typeList[0]);
					idToObject.insert(std::make_pair(object_id, so));
					sceneObjectList.push_back(so);
				}
				else
					so = objectIt->second;
					 
				
				
				objObs->associateToSceneObject(so);
				//ObjectObservation* objObs = new ObjectObservation(color);
				DrawableSceneView* scene = it->second;
				scene->addItem(objObs);
				updatePercent(20 + 80*row/objectObs.size() );
			}
		}
	}
	else
	{
		//LOGERROR
	}
	if(!success)
		std::cout << "Error !\n";
	//maxFrameNumber
	/*InputFrameProviderIface* vfm = getAppContext()->getVFM();
	if(vfm)
		vfm->setNumberFrame(maxFrameNumber);*/
}


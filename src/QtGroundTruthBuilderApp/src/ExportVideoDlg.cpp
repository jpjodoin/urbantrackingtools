#include "ExportVideoDlg.h"
#include <QFileDialog>
#include "AppContext.h"
#include "DrawableSceneView.h"
#include "ObjectObservation.h"
#include "InputFrameProviderIface.h"
#include "SceneObject.h"
#include <map>

#include <opencv2/opencv.hpp>
#include "StatusProgressBar.h"
#include "ExportVideoTask.h"

ExportVideoDlg::ExportVideoDlg(QWidget *parent, StatusProgressBar* progressBar, AppContext* appContext, InputFrameProviderIface* vfm)
: ProgressDialog(progressBar, parent) 
, mContext(appContext)
, m_np_Vfm(vfm)
{
	mUi.setupUi(this);
	mUi.fromTimestamp->setValue(0);
	mUi.toTimestamp->setValue(m_np_Vfm->getNbFrame());
	mUi.fpsBox->setValue(m_np_Vfm->getNbFPS());
	bool connected = connect(mUi.openFileBtnDlg, SIGNAL(clicked()), this, SLOT(ChooseFileDlg()));
}

BaseTask* ExportVideoDlg::getBaseTask() 
{
	QString filePath = mUi.filePathTxtBox->text();
	bool displayTrail = mUi.displayTrailCB->isChecked();
	bool displayBoundingBox = mUi.displayBoundingBoxCB->isChecked();
	bool allTimestamp = mUi.checkBoxAll->isChecked();
	bool sourceFPS = mUi.fpsCB->isChecked();
	int startTime = allTimestamp ? 0 : mUi.fromTimestamp->value();
	int endTime = allTimestamp ? m_np_Vfm->getNbFrame() : mUi.toTimestamp->value();
	float fps = sourceFPS ? m_np_Vfm->getNbFPS() : mUi.fpsBox->value();
	return new ExportVideoTask(getStatusBarProgressControl(), filePath, displayTrail, displayBoundingBox, startTime, endTime, fps, m_np_Vfm, mContext);
}


void ExportVideoDlg::ChooseFileDlg()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Annotated video"),	"", tr("Annotated video(*.avi)"));
	if(fileName != "")
	{
		mUi.filePathTxtBox->setText(fileName);
	}
}
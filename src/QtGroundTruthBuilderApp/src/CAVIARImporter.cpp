#include "CAVIARImporter.h"
#include <QFile>
#include <qdom.h>
#include "DrawableSceneView.h"
#include "SceneObject.h"
#include "ObjectObservation.h"
#include <time.h>
#include "ObjectTypeManager.h"
#include "AppContext.h"
#include "InputVideoFileModule.h"

CaviarImporter::CaviarImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight)
: GTImporter(progressBarControl, appContext, "Caviar XML", "*xml", "CAVIAR", GTImporter::FILE, videoWidth, videoHeight)
{

	

}


CaviarImporter::~CaviarImporter()
{

}


void CaviarImporter::LoadDatabase()
{
	const QString& fileName = getLoadPath();
	int nbFrame = 0;
	std::map<unsigned int, DrawableSceneView*>& sceneMap = getAppContext()->getTimestampToScene();
	QDomDocument doc;
	QFile f(fileName);
	f.open(QIODevice::ReadOnly);
	doc.setContent(&f);
	f.close();
	srand ( time(NULL) ); 
	//We parse the DOM tree
	QDomElement root=doc.documentElement();
	QDomElement child=root.firstChild().toElement();
	std::map<QString, SceneObject*> idToSceneObject;
	updatePercent(10);
	while(!child.isNull())
	{			
		QString timestampStr = child.attribute("number", "0");
		int timestamp = timestampStr.toInt();
		if(timestamp> nbFrame)
			nbFrame = timestamp;
		
		QDomElement grandChild=child.firstChild().toElement();
		while(!grandChild.isNull())
		{
			if (grandChild.tagName() == "objectlist" && !grandChild.childNodes().isEmpty())
			{
				QDomElement objectNode = grandChild.firstChild().toElement();
				while(!objectNode.isNull())
				{
					auto it = sceneMap.find(timestamp);
					if(it == sceneMap.end())
					{
						auto itPair = sceneMap.insert(std::pair<unsigned int, DrawableSceneView*>(timestamp, new DrawableSceneView(timestamp, nullptr, getAppContext())));
						it = itPair.first;
					}
					DrawableSceneView* currentScene = it->second;
					QString id = objectNode.attribute("id", "-1");
					auto objectIt = idToSceneObject.find(id);
					SceneObject* so = nullptr;
					if(objectIt != idToSceneObject.end())
						so = (*objectIt).second;				
					else
					{
						so = new SceneObject(id.toStdString(), "pedestrian", "pedestrian"); //TODO: I could use the role/context to identify maybe
						idToSceneObject.insert(std::make_pair(id,so));
						getAppContext()->getSceneObjectList().push_back(so);
					}

					ObjectObservation* objObs = new ObjectObservation(QColor(0,0,0), false);
					objObs->associateToSceneObject(so);
					QDomElement objectProperty = objectNode.firstChild().toElement();
					bool attributeFound = false;
					while(!objectProperty.isNull() && !attributeFound)
					{
						if(objectProperty.tagName() == "box")
						{
							QString xcStr = objectProperty.attribute("xc", "0");
							QString ycStr = objectProperty.attribute("yc", "0");
							QString hStr = objectProperty.attribute("h", "0");
							QString wStr = objectProperty.attribute("w", "0");
							int xc = xcStr.toInt();
							int yc = ycStr.toInt();
							int h = hStr.toInt();
							int w = wStr.toInt();


							objObs->setRect(QRectF(xc-w/2, yc-h/2, w, h));
							attributeFound = true;
						}
						objectProperty = objectProperty.nextSibling().toElement();
					}
					currentScene->addItem(objObs);
					objectNode = objectNode.nextSibling().toElement();
				}
			}
			grandChild = grandChild.nextSibling().toElement();
			
		}	
		child = child.nextSibling().toElement();
	}
	updatePercent(100);
	InputFrameProviderIface* vfm = getAppContext()->getVFM();
	if(vfm)
		vfm->setNumberFrame(nbFrame);
}

#include "ChangeIdDlg.h"
#include "SceneObject.h"
#include "AppContext.h"
#include "DrawableSceneView.h"
#include "ObjectObservation.h"
ChangeIdDlg::ChangeIdDlg(QWidget *parent, AppContext* appContext)
: QDialog(parent) 
, mContext(appContext)
{
	mUi.setupUi(this);
	mUi.fromList->setSelectionMode(QAbstractItemView::MultiSelection);
	mUi.toList->setSelectionMode(QAbstractItemView::SingleSelection);

	const std::vector<SceneObject*>& objectList =  mContext->getSceneObjectList();
	for(auto it = objectList.begin(); it != objectList.end(); ++it)
	{
		QListWidgetItem* fromItem = new QListWidgetItem(QString(((*it)->getId() + " " + (*it)->getDescription()).c_str()));
		mFromItemToObjectMap[fromItem] = *it;
		mUi.fromList->addItem(fromItem);
		QListWidgetItem* toItem = new QListWidgetItem(QString(((*it)->getId() + " " + (*it)->getDescription()).c_str()));
		mUi.toList->addItem(toItem);
		mToItemToObjectMap[toItem] = *it;
	}

}

void ChangeIdDlg::run()
{
	std::vector<SceneObject*> from;
	SceneObject* into = nullptr;
	int startTime, endTime;

	auto selectedItemList = mUi.fromList->selectedItems();
	for(auto itemIt = selectedItemList.begin(); itemIt != selectedItemList.end(); ++itemIt)	
		from.push_back(mFromItemToObjectMap[(*itemIt)]);
	selectedItemList = mUi.toList->selectedItems();
	if(!selectedItemList.empty())
		into = mToItemToObjectMap[selectedItemList.front()];
	if(into && !from.empty())
	{
		bool allTimestamp = mUi.checkBoxAll->isChecked();
		if(!allTimestamp)
		{
			startTime= mUi.fromTimestamp->value();
			endTime = mUi.toTimestamp->value();
		}
		std::map<SceneObject*, int> nbObservationByObject;
		std::set<SceneObject*> fromObjectSet;
		for(int i = 0; i < from.size(); ++i)
		{
			fromObjectSet.insert(from[i]);
			nbObservationByObject[from[i]] = 0;
		}
		
		auto& timestampMap = mContext->getTimestampToScene();
		for(auto it = timestampMap.begin(); it != timestampMap.end(); ++it)
		{
			int timestamp = (*it).first;
			DrawableSceneView* dsv = (*it).second;
			QList<QGraphicsItem*> items = dsv->items();
			for (int i = 0; i < items.size(); ++i)
			{
				ObjectObservation* obj = dynamic_cast<ObjectObservation*>(items[i]);
				
				if(fromObjectSet.find(obj->getSceneObject()) != fromObjectSet.end())
				{
					if(allTimestamp || (startTime <= timestamp && timestamp <= endTime))
					{
						obj->associateToSceneObject(into);
					}
					else
						++nbObservationByObject[obj->getSceneObject()];
				}	
			}
		}
		std::set<SceneObject*> objectToRemove;
		for(auto it = nbObservationByObject.begin(); it != nbObservationByObject.end(); ++it)
		{
			if((*it).second == 0)			
				objectToRemove.insert((*it).first);			
		}
		std::vector<SceneObject*>& objectList = mContext->getSceneObjectList();
		for(auto it = objectList.begin(); it != objectList.end(); )
		{
			if(objectToRemove.find(*it) != objectToRemove.end())
				it = objectList.erase(it);
			else
				++it;
		}
	}


}
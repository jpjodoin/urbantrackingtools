/*************************************************************************
*	C++ implementation of Evaluating multiple object tracking performance
*   the CLEAR MOT metrics (Keni Bernardin and Rainer Stiefelhagen 2008)
*	
*  Implemented by Jean-Philippe Jodoin
**************************************************************************/
#ifndef CLEAR_METRICS_CALCULATOR_H
#define CLEAR_METRICS_CALCULATOR_H

#include "CLEARMetrics.h"
#include <string>
#include "MeasureDistType.h"

class CLEARDataAssociator;
class Dataset;

class CLEARMetricsCalculator
{
public:
	CLEARMetricsCalculator(const Dataset* groundTruth, const Dataset* experimentalPath, double threshold, MeasureDistType distType);
	~CLEARMetricsCalculator();
	CLEARMetrics calculateMetrics() { return mMetrics;}
	double getMOTP();
	double getMOTA();
	double getMissesRatio();
	double getFPRatio();
	double getMismatchesRatio();
private:
	CLEARDataAssociator* mDataAssociator;
	double mMappingThreshold;
	CLEARMetrics mMetrics;
	MeasureDistType mDistType;
	
};


#endif
#ifndef DATASET_H
#define DATASET_H

#include <map>
#include <vector>
#include <set>
#include "ObjectTypeManager.h"
#include "Observation.h"

class Dataset
{
public:
	Dataset()
	: mLoaded(false)
	{

	}
	virtual ~Dataset()
	{
		clearDataset();
	}

	void clearDataset()
	{
		for(unsigned int i = 0; i < mSceneObject.size(); ++i)
			delete mSceneObject[i]->mSceneObject;
		mSceneObject.clear();
		mLoaded = false;
	}

	const std::vector<SceneObjectWithObservation*>& getSceneObjectList() const {return mSceneObject;}
	const ObjectTypeManager& getObjectTypeManager() const { return mTypeManager;}
	const std::map<unsigned int, std::vector<Observation*>>& getTimestampToObservation() const {return mTimestampToObservation;}
	bool isLoaded() const {return mLoaded;}

	void removeTimestamp(const std::set<int>& timestampToRemove)
	{
		for(auto it = mTimestampToObservation.begin(); it != mTimestampToObservation.end(); )
		{
			if(timestampToRemove.find(it->first) != timestampToRemove.end())
			{
				for(unsigned int i = 0; i < it->second.size(); ++i) //Observation
				{
					for(auto it2 = it->second[i]->mObject->mObservations.begin(); it2  != it->second[i]->mObject->mObservations.end(); ) 
					{
						if( (*it2) == it->second[i])
							it2 =it->second[i]->mObject->mObservations.erase(it2);
						else
							++it2;
					}
					delete it->second[i];					
				}
				it = mTimestampToObservation.erase(it);
			}
			else
				++it;
		}

		for(auto it = mSceneObject.begin(); it != mSceneObject.end(); )
		{
			if((*it)->mObservations.empty())
			{
				SceneObjectWithObservation* so = (*it);				
				it = mSceneObject.erase(it);
				delete so;
			}
			else
				++it;
		}
	}


	void setSceneObjectWithObservationList(std::vector<SceneObjectWithObservation*> objList) 
	{ 

		clearDataset();
		mSceneObject = objList;
		mTimestampToObservation.clear();
		for(unsigned int i = 0; i < mSceneObject.size(); ++i)
		{
			const std::vector<Observation*>& observationList = mSceneObject[i]->mObservations;
			for(unsigned int obsIdx = 0; obsIdx < observationList.size(); ++obsIdx)
			{
				mTimestampToObservation[observationList[obsIdx]->mTimestamp].push_back(observationList[obsIdx]);
			}
		}
		mLoaded = true;
	}

protected:
	std::vector<SceneObjectWithObservation*> mSceneObject;
	std::map<unsigned int, std::vector<Observation*>> mTimestampToObservation;
	ObjectTypeManager mTypeManager;
	bool mLoaded;
};
#endif
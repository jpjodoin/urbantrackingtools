#ifndef PERF_EVAL_OTA_METRICS_H
#define PERF_EVAL_OTA_METRICS_H

#include "Metrics.h"

class PerfEvalOTAMetrics : public Metrics
{
public:
	virtual void displayResults()
	{
		std::cout << "================================= Metrics ============================" << std::endl;	
		std::cout	<< "GT Tracks                                         " << nbGTTracks << std::endl;
		std::cout	<< "System Tracks                                     " << nbSystemTracks << std::endl;
		std::cout	<< "Correct Detected Track (CDT)                      " << CDT << std::endl;
		std::cout	<< "False Alarm Track (FAT)                           " << FAT << std::endl;
		std::cout	<< "Track Detection Failure (TDF)                     " << TDF << std::endl;
		std::cout	<< "Track Fragmentation (TF)                          " << TF << std::endl;
		std::cout	<< "ID Change (IDC)                                   " << IDC << std::endl;
		std::cout	<< "Latency of System track(LT)                       " << LT << std::endl;
		std::cout	<< "Closeness of Track Average (CTM)                  " << CTM << std::endl;
		std::cout	<< "Closeness of Track Deviation (CTD)                " << CTD << std::endl;
		std::cout	<< "Track Matching Error Average (TMEMT)              " << TMEMT << std::endl;
		std::cout	<< "Track Matching Error Deviation(TMEMTD)            " << TMEMTD << std::endl;
		std::cout	<< "Track Completeness Average(TCM)                   " << TCM << std::endl;
		std::cout	<< "Track Completeness Deviation(TCD)                 " << TCD << std::endl;
		std::cout	<< "======================================================================" << std::endl;
	}
	virtual std::string serialize()
	{
		std::stringstream ss;
		ss << "NB_GT_TRACKS;" << nbGTTracks << "\n";
		ss << "NB_SYSTEM_TRACKS;" << nbSystemTracks << "\n";
		ss << "CDT;" << CDT << "\n";
		ss << "FAT;" << FAT << "\n";
		ss << "TDF;" << TDF << "\n";
		ss << "TF;" << TF << "\n";
		ss << "IDC;" << IDC << "\n";
		ss << "LT;" << LT << "\n";
		ss << "CTM;" << CTM << "\n";
		ss << "CDT;" << CDT << "\n";
		ss << "TMEMT;" << TMEMT << "\n";
		ss << "TMEMTD;" << TMEMTD << "\n";
		ss << "TCM;" << TCM << "\n";
		ss << "TCD;" << TCD << "\n";
		return ss.str();
	}

	unsigned int nbGTTracks;
	unsigned int nbSystemTracks;
	unsigned int CDT;
	unsigned int FAT;
	unsigned int TDF;
	unsigned int TF;
	unsigned int IDC;
	double LT;
	double CTM;
	double CTD;
	double TMEMT;
	double TMEMTD;
	double TCM;
	double TCD;
};

#endif
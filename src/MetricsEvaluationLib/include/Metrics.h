#ifndef METRICS_H
#define METRICS_H

#include <string>

class Metrics
{
public:
	virtual ~Metrics(){}
	virtual void displayResults() = 0;
	virtual std::string serialize() = 0;
};


#endif
#include "MetricsHelpers.h"
#include <iostream>
#include <math.h>
#include "Observation.h"



bool MetricsHelpers::temporalOverlap(const std::vector<Observation*>& A, const std::vector<Observation*>& B,  int& startOverlap,  int& endOverlap, int& temporalIntersection, int& temporalUnion)
{
	bool overlap = false;
	if(!(A.empty() || B.empty()))
	{
		int ATimestampStart = A.front()->mTimestamp;
		int ATimestampEnd = A.back()->mTimestamp;
		int BTimestampStart = B.front()->mTimestamp;
		int BTimestampEnd = B.back()->mTimestamp;
		//First Last			
		std::pair<int, int> startTimestamp = ATimestampStart > BTimestampStart ? std::pair<int, int>(BTimestampStart, ATimestampStart) : std::pair< int,  int>(ATimestampStart, BTimestampStart);
		std::pair<int, int> endTimestamp = ATimestampEnd > BTimestampEnd ? std::pair<int, int>(BTimestampEnd, ATimestampEnd) : std::pair<int,  int>(ATimestampEnd, BTimestampEnd);
		startOverlap = startTimestamp.second;
		endOverlap = endTimestamp.first;
		temporalIntersection = endTimestamp.first+1 - startTimestamp.second;
		temporalUnion =  endTimestamp.second+1 - startTimestamp.first;
		overlap = temporalIntersection > 0;

	}
	return overlap;
}



Observation* MetricsHelpers::getObservation(const std::vector<Observation*>& observations, unsigned int timestamp)
{
	Observation* obs = nullptr;
	if(!observations.empty())// && observations.front()->mTimestamp <= timestamp &&  observations.back()->mTimestamp >= timestamp)
	{
		unsigned int startTimestamp = observations.front()->mTimestamp;
		unsigned int endTimestamp = observations.back()->mTimestamp;
		if(timestamp >=startTimestamp && timestamp <= endTimestamp)
		{
			
			if((endTimestamp+1-startTimestamp) == observations.size())
			{
				obs = observations[timestamp-startTimestamp];
			}
			else
			{
				auto it = observations.begin();
				while(it != observations.end() && obs == nullptr)
				{
					if((*it)->mTimestamp == timestamp)
						obs = *it;
					++it;
				}
				//std::cout << "Error: The calculator do not work for discontinuous observations" << std::endl;
			}
		}
	}
	return obs;
}

double MetricsHelpers::spatialOverlap(const Observation* A, const Observation* B)
{
	double spatialOverlap = 0;
	if(A && B)
	{
		spatialOverlap = ((double)Rect::intersection(A->mRectangle, B->mRectangle))/((double)Rect::unionRect(A->mRectangle, B->mRectangle));
	}
	return spatialOverlap;
}


double MetricsHelpers::calculateDistance(Observation* obs1, Observation* obs2, MeasureDistType distType)
{
	double dist = -1;
	switch(distType)
	{
	case ABSOLUTEDIST:
		dist = calculateDistanceAbsolute(obs1, obs2);
		break;
	
	case BOUNDINGBOXOVERLAP:
		dist = 1.-spatialOverlap(obs1, obs2); //0 is a full overlap.
		break;

	default:
		std::cout << "Error unknown distance\n";
		break;
	}
	return dist;
}

double MetricsHelpers::calculateDistanceAbsolute(Observation* obs1, Observation* obs2)
{
	std::pair<double, double> c1 = obs1->mRectangle.getCentroid();
	std::pair<double, double> c2 = obs2->mRectangle.getCentroid();
	double deltaX = c1.first-c2.first;
	double deltaY = c1.second-c2.second;
	return sqrt(deltaX*deltaX+deltaY*deltaY);	
}



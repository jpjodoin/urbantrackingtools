/* Copyright (c) 2014, P.-L. St-Charles (pierre-luc.st-charles@polymtl.ca)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Ecole Polytechnique de Montreal nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

//! init pattern, based on 'floor(fspecial('gaussian',7,1)*4096)'
static const int s_nSamplesInitPatternWidth = 7;
static const int s_nSamplesInitPatternHeight = 7;
static const int s_nSamplesInitPatternTot = 4096;
static const int s_anSamplesInitPattern[s_nSamplesInitPatternHeight][s_nSamplesInitPatternWidth] = {
	{0,     0,     4,     7,     4,     0,     0,},
	{0,    11,    53,    88,    53,    11,     0,},
	{4,    53,   240,   399,   240,    53,     4,},
	{7,    88,   399,   660,   399,    88,     7,},
	{4,    53,   240,   399,   240,    53,     4,},
	{0,    11,    53,    88,    53,    11,     0,},
	{0,     0,     4,     7,     4,     0,     0,},
};

//! returns a random init/sampling position for the specified pixel position; also guards against out-of-bounds values via image/border size check.
static inline void getRandSamplePosition(int& x_sample, int& y_sample, const int x_orig, const int y_orig, const int border, const cv::Size& imgsize) {
	int r = 1+rand()%s_nSamplesInitPatternTot;
	for(x_sample=0; x_sample<s_nSamplesInitPatternWidth; ++x_sample) {
		for(y_sample=0; y_sample<s_nSamplesInitPatternHeight; ++y_sample) {
			r -= s_anSamplesInitPattern[y_sample][x_sample];
			if(r<=0)
				goto stop;
		}
	}
	stop:
	x_sample += x_orig-s_nSamplesInitPatternWidth/2;
	y_sample += y_orig-s_nSamplesInitPatternHeight/2;
	if(x_sample<border)
		x_sample = border;
	else if(x_sample>=imgsize.width-border)
		x_sample = imgsize.width-border-1;
	if(y_sample<border)
		y_sample = border;
	else if(y_sample>=imgsize.height-border)
		y_sample = imgsize.height-border-1;
}

//! simple 8-connected (3x3) neighbors pattern
static const int s_anNeighborPatternSize_3x3 = 8;
static const int s_anNeighborPattern_3x3[8][2] = {
	{-1, 1},  { 0, 1},  { 1, 1},
	{-1, 0},            { 1, 0},
	{-1,-1},  { 0,-1},  { 1,-1},
};

//! returns a random neighbor position for the specified pixel position; also guards against out-of-bounds values via image/border size check.
static inline void getRandNeighborPosition_3x3(int& x_neighbor, int& y_neighbor, const int x_orig, const int y_orig, const int border, const cv::Size& imgsize) {
	int r = rand()%s_anNeighborPatternSize_3x3;
	x_neighbor = x_orig+s_anNeighborPattern_3x3[r][0];
	y_neighbor = y_orig+s_anNeighborPattern_3x3[r][1];
	if(x_neighbor<border)
		x_neighbor = border;
	else if(x_neighbor>=imgsize.width-border)
		x_neighbor = imgsize.width-border-1;
	if(y_neighbor<border)
		y_neighbor = border;
	else if(y_neighbor>=imgsize.height-border)
		y_neighbor = imgsize.height-border-1;
}

//! 5x5 neighbors pattern
static const int s_anNeighborPatternSize_5x5 = 24;
static const int s_anNeighborPattern_5x5[24][2] = {
	{-2, 2},  {-1, 2},  { 0, 2},  { 1, 2},  { 2, 2},
	{-2, 1},  {-1, 1},  { 0, 1},  { 1, 1},  { 2, 1},
	{-2, 0},  {-1, 0},            { 1, 0},  { 2, 0},
	{-2,-1},  {-1,-1},  { 0,-1},  { 1,-1},  { 2,-1},
	{-2,-2},  {-1,-2},  { 0,-2},  { 1,-2},  { 2,-2},
};

//! returns a random neighbor position for the specified pixel position; also guards against out-of-bounds values via image/border size check.
static inline void getRandNeighborPosition_5x5(int& x_neighbor, int& y_neighbor, const int x_orig, const int y_orig, const int border, const cv::Size& imgsize) {
	int r = rand()%s_anNeighborPatternSize_5x5;
	x_neighbor = x_orig+s_anNeighborPattern_3x3[r][0];
	y_neighbor = y_orig+s_anNeighborPattern_3x3[r][1];
	if(x_neighbor<border)
		x_neighbor = border;
	else if(x_neighbor>=imgsize.width-border)
		x_neighbor = imgsize.width-border-1;
	if(y_neighbor<border)
		y_neighbor = border;
	else if(y_neighbor>=imgsize.height-border)
		y_neighbor = imgsize.height-border-1;
}

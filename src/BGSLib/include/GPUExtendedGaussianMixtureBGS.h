#ifndef GPU_EXTENDED_GAUSSIAN_MIXTURE_BGS_H
#define GPU_EXTENDED_GAUSSIAN_MIXTURE_BGS_H

#ifdef CUDA

#include <iostream>
#include <opencv2/opencv.hpp>
#include <highgui.h>

#include "IBGS.h"
#define CUDAGMM_VERSION 4

#include "CvFastBgGMM.h"

class GPUExtendedGaussianMixtureBGS : public IBGS
{

public:
	GPUExtendedGaussianMixtureBGS();
	~GPUExtendedGaussianMixtureBGS();

	void process(const cv::Mat &img_input, cv::Mat &img_output);
	virtual int getNbChannel() { return 3; }; 
private:
	void saveConfig();
	void loadConfig();

	CvFastBgGMMParams* pGMMParams;
	CvFastBgGMM* pGMM;
};
#endif

#endif